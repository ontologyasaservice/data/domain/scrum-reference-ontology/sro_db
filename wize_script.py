from sro_db.application import factories as application_factory
from sro_db.model import factories as model_factory


#Criando os dados do prodest
organization = model_factory.OrganizationFactory()
organization.name = "Wize"
organization.description = "Wize Company"

organization_application = application_factory.OrganizationFactory()
organization = organization_application.create(organization)

applicationService = application_factory.ApplicationFactory()
jira = applicationService.retrive_by_name("jira")

#criando a applicação
configuration = model_factory.ConfigurationFactory()
configuration.name = "Configuação da Wize para o Jira"
configuration.description = "Configuação da Wize para o Jira"
configuration.user = "paulossjunior@gmail.com"
configuration.secret = "vbzmIZMGIKNZ9zqDtUfe01DB"
configuration.url =  "https://wizecompany.atlassian.net/"
# configuration.user = "lucasmoraesplay@gmail.com"
# configuration.secret = "seApnbFfBXp6AVCdanCK8DFB"
# configuration.url = "https://ledszeppellin.atlassian.net/"
configuration.application = jira
configuration.organization = organization

configuration_application = application_factory.ConfigurationFactory()
configuration = configuration_application.create(configuration)

print('data = {')
print(f"    'user': '{configuration.user}',")
print(f"    'key': '{configuration.secret}',")
print(f"    'url': '{configuration.url}',")
print(f"    'organization_id': '{str(organization.uuid)}',")
print(f"    'configuration_id': '{str(configuration.uuid)}',")
print("}")
print(f"http://10.3.152.112:8090/api/jira/sro/{str(organization.uuid)}/{str(configuration.uuid)}")
