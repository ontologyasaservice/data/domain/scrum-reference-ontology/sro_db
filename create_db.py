from sro_db.config.config import Config
from sro_db.model.activity.models import *
from sro_db.service.activity.service import *
from sro_db.model.core.models import *
from sro_db.service.core.service import *
from sro_db.model.stakeholder_participation.models import *
from sro_db.application import factories as factories_application
from sro_db.model import factories as factories_model

conf = Config()
conf.create_database()

risks = ["high", "medium", "normal"]

activity_types = ["deployment", "analysis", "design", "development", "documentation", "requirements", "testing"]

developmentTaskTypeService_application = factories_application.DevelopmentTaskTypeFactory() #DevelopmentTaskTypeService()

task_type_list = []

for data in activity_types:

    task_type = factories_model.DevelopmentTaskTypeFactory() #DevelopmentTaskType()
    
    task_type.name = data
    task_type.description = data
    task_type_list.append(task_type)


developmentTaskTypeService_application.create_bulk(task_type_list)
#fechando a conexão com o banco
developmentTaskTypeService_application.close_session_connection()


priority_application = factories_application.PriorityFactory()
risk_application = factories_application.RiskFactory()

priority_list = []
risk_list = []
for data in risks:

    priority = factories_model.PriorityFactory()
    
    priority.name = data
    priority.description = data
    priority_list.append (priority)
    
    risk = factories_model.RiskFactory()
    risk.name = data
    risk.description = data
    risk_list.append (risk)
   
priority_application.create_bulk(priority_list)
risk_application.create_bulk(risk_list)


application_type_application = factories_application.ApplicationTypeFactory()


project_management = factories_model.ApplicationTypeFactory()
project_management.name = "Project Management"
project_management.description = "Project Management"
application_type_application.create(project_management)

time_tracking_management = factories_model.ApplicationTypeFactory()
time_tracking_management.name = "Time Tracking Management"
time_tracking_management.description = "Time Tracking Management"
application_type_application.create(time_tracking_management)

application_service_application = factories_application.ApplicationFactory()

#Criando o TFS
tfs = factories_model.ApplicationFactory()
tfs.name = "tfs"
tfs.description = "tfs"
tfs.application_type = project_management
application_service_application.create(tfs)

#Criando o Jira
jira = factories_model.ApplicationFactory()
jira.name = "jira"
jira.description = "jira"
jira.application_type = project_management
application_service_application.create(jira)

#Criando o clockify
clockify = factories_model.ApplicationFactory()
clockify.name = "clockify"
clockify.description = "clockify"
clockify.application_type = time_tracking_management
application_service_application.create(clockify)


