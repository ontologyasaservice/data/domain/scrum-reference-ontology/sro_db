from sspo_db.model.organization.models import *
from sspo_db.model.core.models import *
from sspo_db.service.organization.service import *
from sspo_db.service.core.service import *
from pprint import pprint 

#Criando os dados do prodest
organization = Organization()
organization.name = "Prodest"
organization.description = "Instituto de Processamento de dados do ES"

organizationService = OrganizationService()
organizationService.create(organization)

applicationService = ApplicationService()
tfs = applicationService.retrive_by_name("tfs")

#criando a applicação
configuration = Configuration()
configuration.name = "Configuação do Prodest para o TFS"
configuration.description = "Configuação do Prodest para o TFS"
configuration.secret = "gdfwj4xwjqgvehxx3woomf7b54lvqjiuyfttj2vtq2pbsr2k77wq"
configuration.url =  "https://tfs.es.gov.br/tfs/DefaultCollection/"
configuration.application = tfs
configuration.organization = organization

configurationService = ConfigurationService()
configurationService.create(configuration)

clockify = applicationService.retrive_by_name("clockify")

#criando a applicação
configuration = Configuration()
configuration.name = "Configuação do Prodest para o Clockify"
configuration.description = "Configuação do Prodest para o Clockify"
configuration.secret = "XcN5OMhbvm/92koz"
configuration.url =  ""
configuration.application = clockify
configuration.organization = organization



