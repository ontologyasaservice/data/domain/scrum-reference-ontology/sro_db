from sspo_db.model.organization.models import *
from sspo_db.service.organization.service import *
from sspo_db.model.core.models import *

from sspo_db.service.core.service import *
from sspo_db.model.process.models import ScrumAtomicProject
from sspo_db.service.process.service import ScrumAtomicProjectService
from pprint import pprint 

#Criando os dados do prodest
organization = Organization()
organization.name = "NEMO"
organization.description = "Nemo"

organizationService = OrganizationService()
organizationService.create(organization)

applicationService = ApplicationService()
tfs = applicationService.retrive_by_name("tfs")

#criando a applicação
configuration = Configuration()
configuration.name = "Configuação do NEMO para o TFS"
configuration.description = "Configuação do NEMO para o TFS"
configuration.secret = "l5qfcsgiduaxytcgwjqmq5y4lylfhkmrwwqq4kt2vh2ipfsvpfia"
configuration.url =  "https://dev.azure.com/paulossjunior/"
configuration.application = tfs
configuration.organization = organization

configurationService = ConfigurationService()
configurationService.create(configuration)
